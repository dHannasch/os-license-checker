# os-license-checker

WARNING: This is alpha and not very good, but it works.

## What?

A GitLab CI pipeline that checks all projects in a group namespace for OSI-approved licenses.

## Usage

1. Get `group_id` for top-level group namespace by navigating to `gitlab.com/<group_name>`. Copy this Group ID.
2. In the `os-license-checker` project, Go to CI/CD > Pipeline. Click Run Pipeline button.
3. Enter the following variable: `GROUP_ID`: `<group_id_copied_earlier>`
4. :tada:

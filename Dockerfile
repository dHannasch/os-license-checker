FROM ubuntu:22.04

RUN apt-get update && apt-get upgrade -y && apt-get install -y -q \
    ca-certificates \
    curl \
    jq && \
    curl -L https://gitlab.com/greg/scripts/-/raw/master/glab-install.sh | bash && \
    rm -rf /var/lib/apt/lists/* /var/cache/debconf/templates.* /var/lib/dpkg/status-old
